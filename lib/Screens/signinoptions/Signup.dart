import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:udemy_clone/Services/Authentication.dart';

import 'Signin.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  Authentication authentication = Authentication();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 100.0),
                child: Text(
                  'Sign up',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 35.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: FlatButton.icon(
                  minWidth: 200.0,
                  color: Colors.white,
                  onPressed: () async {
                    await authentication.googleSignIn();
                  },
                  icon: Icon(EvaIcons.email),
                  label: Text('Sign in Email'),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        width: 150.0,
                        child: Divider(
                          color: Colors.white54,
                        ),
                      ),
                      Text(
                        'or',
                        style: TextStyle(color: Colors.white),
                      ),
                      SizedBox(
                        width: 150.0,
                        child: Divider(
                          color: Colors.white54,
                        ),
                      ),
                    ],
                  )),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: FlatButton.icon(
                  minWidth: 200.0,
                  color: Colors.white,
                  onPressed: () async {
                    await authentication.googleSignIn();
                  },
                  icon: Icon(EvaIcons.google),
                  label: Text('Sign in with Google'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: FlatButton.icon(
                  minWidth: 200.0,
                  color: Colors.white,
                  onPressed: () {
                    return null;
                  },
                  icon: Icon(EvaIcons.facebook),
                  label: Text('Sign in with Facebook'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: FlatButton.icon(
                  minWidth: 200.0,
                  color: Colors.white,
                  onPressed: () {
                    return null;
                  },
                  icon: Icon(FontAwesomeIcons.apple),
                  label: Text('Sign in with Apple'),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Already have an account?',
                    style: TextStyle(color: Colors.white),
                  ),
                  MaterialButton(
                    onPressed: () {
                      Navigator.pushReplacement(
                          context,
                          PageTransition(
                              child: SignIn(),
                              type: PageTransitionType.bottomToTop));
                    },
                    child: Text(
                      'Log In',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Text(
                  'Terms of Use',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        ),
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage(
            'images/landingpagebg.png',
          ),
          fit: BoxFit.fill,
          colorFilter: ColorFilter.mode(Colors.black54, BlendMode.darken),
        )),
      ),
    );
  }
}

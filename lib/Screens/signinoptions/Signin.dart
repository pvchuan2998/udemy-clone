import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:udemy_clone/Screens/signinoptions/Signup.dart';
import 'package:udemy_clone/Services/Authentication.dart';

import '../HomeScreen.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  Authentication authentication = Authentication();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 100.0),
                child: Text(
                  'Sign in',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 35.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: FlatButton.icon(
                  minWidth: 200.0,
                  color: Colors.white,
                  onPressed: () async {
                    await authentication.googleSignIn();
                  },
                  icon: Icon(EvaIcons.email),
                  label: Text('Sign in Email'),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        width: 150.0,
                        child: Divider(
                          color: Colors.white54,
                        ),
                      ),
                      Text(
                        'or',
                        style: TextStyle(color: Colors.white),
                      ),
                      SizedBox(
                        width: 150.0,
                        child: Divider(
                          color: Colors.white54,
                        ),
                      ),
                    ],
                  )),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: FlatButton.icon(
                  minWidth: 200.0,
                  color: Colors.white,
                  onPressed: () async{
                    await authentication.googleSignIn().whenComplete(() {
                      Navigator.pushReplacement(context, PageTransition(child: HomeScreen(), type: PageTransitionType.rightToLeftWithFade));
                    });
                  },
                  icon: Icon(EvaIcons.google),
                  label: Text('Sign in with Google'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: FlatButton.icon(
                  minWidth: 200.0,
                  color: Colors.white,
                  onPressed: () {
                    return null;
                  },
                  icon: Icon(EvaIcons.facebook),
                  label: Text('Sign in with Facebook'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: FlatButton.icon(
                  minWidth: 200.0,
                  color: Colors.white,
                  onPressed: () {
                    return null;
                  },
                  icon: Icon(FontAwesomeIcons.apple),
                  label: Text('Sign in with Apple'),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'New here?',
                    style: TextStyle(color: Colors.white),
                  ),
                  MaterialButton(onPressed: () {
                    Navigator.pushReplacement(
                        context,
                        PageTransition(
                            child: SignUp(),
                            type: PageTransitionType.bottomToTop));
                  },
                    child: Text('Create an account', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Text(
                  'Terms of Use',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        ),
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage(
            'images/landingpagebg.png',
          ),
          fit: BoxFit.fill,
          colorFilter: ColorFilter.mode(Colors.black54, BlendMode.darken),
        )),
      ),
    );
  }
}

import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:udemy_clone/Screens/DetailSection/DetailScreen.dart';
import 'package:udemy_clone/Screens/MyList.dart';
import 'package:udemy_clone/Services/DataController.dart';

class Featured extends StatefulWidget {
  @override
  _FeaturedState createState() => _FeaturedState();
}

class _FeaturedState extends State<Featured> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(
                EvaIcons.shoppingCartOutline,
                color: Colors.white,
              ),
              onPressed: () {
                Get.to(MyList());
              })
        ],
        backgroundColor: Colors.black,
        title: Text(
          'Featured',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 200.0,
                width: 400.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage('images/salebanner.png'))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 80.0,
                width: 400.0,
                color: Colors.blueAccent,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('Courses now on sale',
                        style: TextStyle(color: Colors.white, fontSize: 22.0)),
                    Text('1 Day Left',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 24.0,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Text(
                'Featured',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 320.0,
              width: 400.0,
              child: GetBuilder<DataController>(
                init: DataController(),
                builder: (value) {
                  return FutureBuilder(
                    future: value.getData('featured'),
                    builder: (context, snap) {
                      if (snap.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.black,
                          ),
                        );
                      } else {
                        return ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: snap.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap: () {
                                Get.to(DetailScreen(), transition: Transition.leftToRightWithFade, arguments: snap.data[index]);
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 130.0,
                                        width: 200.0,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            image: DecorationImage(
                                                fit: BoxFit.fill,
                                                image: NetworkImage(snap
                                                    .data[index]
                                                    .data()['image']))),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 8.0),
                                        child: Container(
                                          constraints:
                                              BoxConstraints(maxWidth: 200.0),
                                          child: Text(
                                              snap.data[index].data()['title'],
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 18.0),
                                              textAlign: TextAlign.start),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 8.0),
                                        child: Text(
                                          snap.data[index].data()['author'],
                                          style: TextStyle(
                                              color: Colors.grey, fontSize: 12.0),
                                        ),
                                      ),
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            EvaIcons.star,
                                            color: Colors.limeAccent,
                                          ),
                                          Icon(
                                            EvaIcons.star,
                                            color: Colors.limeAccent,
                                          ),
                                          Icon(
                                            EvaIcons.star,
                                            color: Colors.limeAccent,
                                          ),
                                          Icon(
                                            EvaIcons.star,
                                            color: Colors.limeAccent,
                                          ),
                                          Icon(
                                            EvaIcons.star,
                                            color: Colors.limeAccent,
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(left: 5.0),
                                            child: Text(
                                              snap.data[index].data()['rating'],
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 17.0),
                                            ),
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(left: 5.0),
                                            child: Text(
                                              ('(${snap.data[index].data()['enroll']})'),
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 17.0),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            snap.data[index].data()['price'],
                                            style: TextStyle(
                                                color: Colors.white, fontSize: 20.0),
                                          ),
                                          Icon(FontAwesomeIcons.dollarSign),
                                        ],
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                          color: Colors.limeAccent,
                                          borderRadius: BorderRadius.circular(7.0),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text('${snap.data[index].data()['tag']}'),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      }
                    },
                  );
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Row(
                children: [
                  Text(
                    'Top courses in',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(width: 3.0,),
                  Text(
                    'Development',
                    style: TextStyle(
                        color: Colors.blueGrey,
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 320.0,
              width: 400.0,
              child: GetBuilder<DataController>(
                init: DataController(),
                builder: (value) {
                  return FutureBuilder(
                    future: value.getData('featured'),
                    builder: (context, snap) {
                      if (snap.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.black,
                          ),
                        );
                      } else {
                        return ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: snap.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      height: 130.0,
                                      width: 200.0,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                          BorderRadius.circular(10.0),
                                          image: DecorationImage(
                                              fit: BoxFit.fill,
                                              image: NetworkImage(snap
                                                  .data[index]
                                                  .data()['image']))),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: Container(
                                        constraints:
                                        BoxConstraints(maxWidth: 200.0),
                                        child: Text(
                                            snap.data[index].data()['title'],
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 18.0),
                                            textAlign: TextAlign.start),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: Text(
                                        snap.data[index].data()['author'],
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 12.0),
                                      ),
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                          EvaIcons.star,
                                          color: Colors.limeAccent,
                                        ),
                                        Icon(
                                          EvaIcons.star,
                                          color: Colors.limeAccent,
                                        ),
                                        Icon(
                                          EvaIcons.star,
                                          color: Colors.limeAccent,
                                        ),
                                        Icon(
                                          EvaIcons.star,
                                          color: Colors.limeAccent,
                                        ),
                                        Icon(
                                          EvaIcons.star,
                                          color: Colors.limeAccent,
                                        ),
                                        Padding(
                                          padding:
                                          const EdgeInsets.only(left: 5.0),
                                          child: Text(
                                            snap.data[index].data()['rating'],
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 17.0),
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                          const EdgeInsets.only(left: 5.0),
                                          child: Text(
                                            ('(${snap.data[index].data()['enroll']})'),
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 17.0),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          snap.data[index].data()['price'],
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 20.0),
                                        ),
                                        Icon(FontAwesomeIcons.dollarSign),
                                      ],
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        color: Colors.limeAccent,
                                        borderRadius: BorderRadius.circular(7.0),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text('${snap.data[index].data()['tag']}'),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      }
                    },
                  );
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Row(
                children: [
                  Text(
                    'Top courses in',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(width: 3.0,),
                  Text(
                    'IT & Software',
                    style: TextStyle(
                        color: Colors.blueGrey,
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 320.0,
              width: 400.0,
              child: GetBuilder<DataController>(
                init: DataController(),
                builder: (value) {
                  return FutureBuilder(
                    future: value.getData('featured'),
                    builder: (context, snap) {
                      if (snap.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.black,
                          ),
                        );
                      } else {
                        return ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: snap.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      height: 130.0,
                                      width: 200.0,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(8.0),
                                          image: DecorationImage(
                                              fit: BoxFit.fill,
                                              image: NetworkImage(snap
                                                  .data[index]
                                                  .data()['image']))),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: Container(
                                        constraints:
                                        BoxConstraints(maxWidth: 200.0),
                                        child: Text(
                                            snap.data[index].data()['title'],
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 18.0),
                                            textAlign: TextAlign.start),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: Text(
                                        snap.data[index].data()['author'],
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 12.0),
                                      ),
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                          EvaIcons.star,
                                          color: Colors.limeAccent,
                                        ),
                                        Icon(
                                          EvaIcons.star,
                                          color: Colors.limeAccent,
                                        ),
                                        Icon(
                                          EvaIcons.star,
                                          color: Colors.limeAccent,
                                        ),
                                        Icon(
                                          EvaIcons.star,
                                          color: Colors.limeAccent,
                                        ),
                                        Icon(
                                          EvaIcons.star,
                                          color: Colors.limeAccent,
                                        ),
                                        Padding(
                                          padding:
                                          const EdgeInsets.only(left: 5.0),
                                          child: Text(
                                            snap.data[index].data()['rating'],
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 17.0),
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                          const EdgeInsets.only(left: 5.0),
                                          child: Text(
                                            ('(${snap.data[index].data()['enroll']})'),
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 17.0),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          snap.data[index].data()['price'],
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 20.0),
                                        ),
                                        Icon(FontAwesomeIcons.dollarSign),
                                      ],
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        color: Colors.limeAccent,
                                        borderRadius: BorderRadius.circular(7.0),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text('${snap.data[index].data()['tag']}'),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      }
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

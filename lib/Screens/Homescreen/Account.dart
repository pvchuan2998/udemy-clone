import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:udemy_clone/Screens/landingPage.dart';
import 'package:udemy_clone/Services/Authentication.dart';
import 'package:udemy_clone/Services/Storage.dart';

import '../splashScreen.dart';

class Account extends StatefulWidget {
  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  Authentication authentication = Authentication();
  SecureStorage secureStorage = SecureStorage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(
                EvaIcons.shoppingCartOutline,
                color: Colors.white,
              ),
              onPressed: null)
        ],
        backgroundColor: Colors.black,
        title: Text(
          'Account',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 200.0,
              width: 400.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    finalName,
                    style: TextStyle(color: Colors.white, fontSize: 24.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          EvaIcons.google,
                          color: Colors.white,
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          finalEmail,
                          style: TextStyle(color: Colors.grey, fontSize: 20.0),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  MaterialButton(
                    onPressed: () {},
                    child: Text(
                      'Become an instructor',
                      style: TextStyle(
                          color: Colors.lightBlue, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15.0),
              child: Text(
                'Video preference',
                style: TextStyle(color: Colors.grey),
              ),
            ),
            ListTile(
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              ),
              title: Text(
                'Download options',
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
            ),
            ListTile(
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              ),
              title: Text(
                'Video playback options',
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15.0, top: 30.0),
              child: Text(
                'Account settings',
                style: TextStyle(color: Colors.grey),
              ),
            ),
            ListTile(
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              ),
              title: Text(
                'Account security',
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
            ),
            ListTile(
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              ),
              title: Text(
                'Email notification preferences',
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
            ),
            ListTile(
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              ),
              title: Row(
                children: [
                  Text(
                    'Learning reminders',
                    style: TextStyle(color: Colors.white, fontSize: 20.0),
                  ),
                  SizedBox(
                    width: 5.0,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7.0),
                      color: Colors.greenAccent,
                    ),
                    alignment: Alignment.center,
                    width: 50.0,
                    height: 30.0,
                    child: Text(
                      'NEW',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15.0, top: 30.0),
              child: Text(
                'Support',
                style: TextStyle(color: Colors.grey),
              ),
            ),
            ListTile(
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              ),
              title: Text(
                'About Udemy',
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
            ),
            ListTile(
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              ),
              title: Text(
                'About Udemy for Business',
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
            ),
            ListTile(
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              ),
              title: Text(
                'Frequently asked questions',
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
            ),
            ListTile(
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              ),
              title: Text(
                'Share the Udemy app',
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15.0, top: 30.0),
              child: Text(
                'Diagnostics',
                style: TextStyle(color: Colors.grey),
              ),
            ),
            ListTile(
              trailing: Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              ),
              title: Text(
                'Status',
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: GestureDetector(
                  onTap: () async {
                    await authentication.googleSignOut().whenComplete((){
                      secureStorage.deleteSecureData('email');
                      secureStorage.deleteSecureData('name');
                    }).whenComplete(() {
                      Navigator.pushReplacement(context, PageTransition(child: LandingPage(), type: PageTransitionType.bottomToTop));
                    });
                  },
                  child: Text(
                    'Sign out',
                    style: TextStyle(color: Colors.blueGrey, fontSize: 20.0),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: Text(
                  'Udemy v6.9.0',
                  style: TextStyle(color: Colors.grey),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

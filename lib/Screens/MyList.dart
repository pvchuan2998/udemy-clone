import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:udemy_clone/Screens/HomeScreen.dart';
import 'package:udemy_clone/Screens/Homescreen/Wishlist.dart';

class MyList extends StatefulWidget {
  @override
  _MyListState createState() => _MyListState();
}

class _MyListState extends State<MyList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white,),
          onPressed: () {
            Navigator.pushReplacement(context, PageTransition(child: HomeScreen(), type: PageTransitionType.leftToRightWithFade));
          },
        ),
        backgroundColor: Colors.black,
        title: Text(
          'My List',
          style: TextStyle(color: Colors.white),
        ),
        actions: [
          IconButton(
              icon: Icon(
                EvaIcons.shoppingCartOutline,
                color: Colors.white,
              ),
              onPressed: () {})
        ],
      ),
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: 200.0,
              width: 400.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircleAvatar(
                    child: Icon(
                      EvaIcons.shakeOutline,
                      color: Colors.white,
                    ),
                    radius: 50.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Text(
                      'Want to save something for later',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 24.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Text(
                      'Your wishlist shall go here',
                      style: TextStyle(
                          color: Colors.grey.shade800,
                          fontSize: 18.0,),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

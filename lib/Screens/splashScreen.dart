import 'dart:async';

import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:udemy_clone/Screens/HomeScreen.dart';
import 'package:udemy_clone/Screens/landingPage.dart';
import 'package:udemy_clone/Services/Storage.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

String finalEmail, finalName;

class _SplashScreenState extends State<SplashScreen> {
  final SecureStorage secureStorage = SecureStorage();
  @override
  void initState() {

    secureStorage.readSecureData('email').then((value) {
      finalEmail = value;
    });

    secureStorage.readSecureData('name').then((value) {
      finalName = value;
    });

    Timer(
        Duration(seconds: 3),
        () => Navigator.pushReplacement(
            context,
            PageTransition(
                child: finalEmail == null ? LandingPage() : HomeScreen(),
                type: PageTransitionType.bottomToTop)));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 200.0,
                child: Image.asset(
                  'images/logo.png',
                  height: 180,
                  width: 180,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 200.0),
                child: CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

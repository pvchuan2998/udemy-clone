import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:udemy_clone/Screens/Homescreen/Account.dart';
import 'package:udemy_clone/Screens/Homescreen/Featured.dart';
import 'package:udemy_clone/Screens/Homescreen/MyCourses.dart';
import 'package:udemy_clone/Screens/Homescreen/Search.dart';
import 'package:udemy_clone/Screens/Homescreen/Wishlist.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  PageController pageController = PageController();
  int currentIndex = 0;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String _message = '';
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 1), () {
      _firebaseMessaging.configure(
          onLaunch: (Map<String, dynamic> message) async {
        setState(() {
          _message = message['title'];
        });
      }, onResume: (Map<String, dynamic> message) async {
        setState(() {
          _message = message['title'];
        });
      }, onMessage: (Map<String, dynamic> message) async {
        setState(() {
          _message = message['title'];
        });
      });
      _firebaseMessaging.getToken().then((value) {
        print(value);
      });
    });
  }

  void onTap(int page) {
    setState(() {
      currentIndex = page;
    });
    pageController.jumpToPage(page);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        onPageChanged: (index) {
          setState(() {
            currentIndex = index;
          });
        },
        controller: pageController,
        children: [Featured(), Search(), MyCourses(), Wishlist(), Account()],
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTap,
        backgroundColor: Colors.black87,
        selectedIconTheme: IconThemeData(
          color: Colors.redAccent,
        ),
        unselectedIconTheme: IconThemeData(color: Colors.white),
        unselectedLabelStyle: TextStyle(color: Colors.white),
        unselectedItemColor: Colors.white,
        iconSize: 26.0,
        selectedFontSize: 14.0,
        unselectedFontSize: 12.0,
        currentIndex: currentIndex,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.star_border_outlined), title: Text('Featured')),
          BottomNavigationBarItem(
              icon: Icon(Icons.search), title: Text('Search')),
          BottomNavigationBarItem(
              icon: Icon(Icons.video_collection_rounded),
              title: Text('My Courses')),
          BottomNavigationBarItem(
              icon: Icon(EvaIcons.heartOutline), title: Text('Wishlist')),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_box_rounded), title: Text('Account')),
        ],
      ),
    );
  }
}

# Flutter Udemy clone

Udemy Clone Flutter application Using GetX State Management Package.

Key features:
1. Login/Logout, Google SignIn
2. Beautiful UI (splash screen, login/register screen, search, wishlist, detailed courses...)
3. Video preview
4. Add courses to Wishlist
5. Search Courses
6. Razorpay
...
# Screenshot
![logo](/uploads/65fcf700b381a32bc690064989d51910/logo.png)

<img src="/uploads/be2bf158ed9d1ba9ddea533e3d0ac19f/Screenshot_20210326-205647.png" width="400" height="600">
<img src="/uploads/8dd2efe5e8024cbce53cf23b208078b4/Screenshot_20210326-205652.png" width="400" height="600">
<img src="/uploads/c094b7068db2db63dd2c6c6ab3e83bb4/Screenshot_20210326-205016.png" width="400" height="600">
<img src="/uploads/586dabb199de4d34894cc39f25f40dc9/Screenshot_20210326-205116.png" width="400" height="600">
<img src="/uploads/b25034f93e2c00646bbb2d7243708020/Screenshot_20210326-205041.png" width="400" height="600">
